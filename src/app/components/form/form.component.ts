import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  constructor(private fb: FormBuilder) {}
  public statesGroup: FormGroup;

  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.statesGroup = this.fb.group({
      states: this.fb.array([
        this.fb.control(null),
        this.fb.control(null),
      ]),
    });

  }

  get statesArray() {
    return this.statesGroup.get('states') as FormArray;
  }
}
