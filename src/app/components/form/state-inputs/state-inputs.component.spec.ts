import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateInputsComponent } from './state-inputs.component';

describe('StateInputsComponent', () => {
  let component: StateInputsComponent;
  let fixture: ComponentFixture<StateInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
