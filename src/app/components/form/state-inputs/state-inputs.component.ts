import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-state-inputs',
  templateUrl: './state-inputs.component.html',
  styleUrls: ['./state-inputs.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StateInputsComponent),
      multi: true,
    },
  ],
})
export class StateInputsComponent {
  value: { value1: string; value2: string };
  isDisabled: boolean;

  onChanged: any = () => {};
  onTouched: any = () => {};

  writeValue(data: any): void {
    console.log(data);
    this.value = {
      value1: data && data.value1 || '',
      value2: data && data.value2 || '',
    };
  }

  onInput1(value) {
    this.value = {
      ...this.value,
      value1: value,
    };
    this.onChanged(this.value);
  }

  onInput2(value) {
    this.value = {
      ...this.value,
      value2: value,
    };
    this.onChanged(this.value);
  }

  registerOnChange(fn: any) {
    this.onChanged = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
